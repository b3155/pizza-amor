//COMPONENT
import { Container } from "@/ui/components/container/container";
import { Layout } from "@/ui/components/layout/layout";
import { Seo } from "@/ui/components/seo/seo";

//DESIGN-SYSTEM
import { Avatar } from "@/ui/design-system/avatar/avatar";
import { Button } from "@/ui/design-system/button/button";
import { Logo } from "@/ui/design-system/logo/logo";
import { Spinner } from "@/ui/design-system/spinner/spinner";
import { Typography } from "@/ui/design-system/typography/typography";

//ICO
import { RiUser6Fill } from "react-icons/ri";

export default function DesignSystem() {

  return (
    <>
      <Seo title="Design System" description="Description ..." />

      <Layout>
        <Container className="py-10 space-y-10">
              {/* Typography */}
        <div className="space-y-2">
        <Typography variant="caption2" weight="medium">
          Typography
        </Typography>
        <div className="flex flex-col gap-2 p-5 border border-gray-400 divide-y-2 divide-gray-400        rounded">
          <div className="pb-5 space-y-2">
            <Typography variant="caption3" weight="medium">
              Display
            </Typography>
            <Typography variant="display">
              Nothing is impossible
            </Typography>
          </div>
        
          <div className="pb-5 space-y-2">
            <Typography variant="caption3" weight="medium">
              H1
            </Typography>
            <Typography variant="h1">
              Nothing is impossible, the word itself says, I'm possible!
            </Typography>
          </div>
        
          <div className="pb-5 space-y-2">
            <Typography variant="caption3" weight="medium">
              H2
            </Typography>
            <Typography variant="h2">
              Your time is limited, so don't waste it living someone else's life
            </Typography>
          </div>
        
          <div className="pb-5 space-y-2">
            <Typography variant="caption3" weight="medium">
              H3
            </Typography>
            <Typography variant="h3">
              Daily Report:Removing Checks to the Power of the Internet Titans
            </Typography>
          </div>
        
          <div className="pb-5 space-y-2">
            <Typography variant="caption3" weight="medium">
              H4
            </Typography>
            <Typography variant="h4">
              Daily Report:Removing Checks to the Power of the Internet Titans
            </Typography>
          </div>
        
          <div className="pb-5 space-y-2">
            <Typography variant="caption3" weight="medium">
              H5
            </Typography>
            <Typography variant="h5">
              Daily Report:Removing Checks to the Power of the Internet Titans
            </Typography>
          </div>
        
          <div className="pb-5 space-y-2">
            <Typography variant="caption3" weight="medium">
              Lead
            </Typography>
            <Typography variant="lead">
              Daily Report:Removing Checks to the Power of the Internet Titans
            </Typography>
          </div>
        
          <div className="pb-5 space-y-2">
            <Typography variant="caption3" weight="medium">
              Body lg
            </Typography>
            <Typography variant="body-lg">
              Daily Report:Removing Checks to the Power of the Internet Titans
            </Typography>
          </div>
        
          <div className="pb-5 space-y-2">
            <Typography variant="caption3" weight="medium">
              Body base
            </Typography>
            <Typography variant="body-base">
              Daily Report:Removing Checks to the Power of the Internet Titans
            </Typography>
          </div>
        
          <div className="pb-5 space-y-2">
            <Typography variant="caption3" weight="medium">
              Body sm
            </Typography>
            <Typography variant="body-sm">
              Daily Report:Removing Checks to the Power of the Internet Titans
            </Typography>
          </div>
        
          <div className="flex flex-wrap space-x-2">
            <div className="p-5 space-y-2 border-r pr-7 border-gray-400">
              <Typography variant="caption3" weight="medium">
                Caption1
              </Typography>
              <Typography variant="caption1" weight="regular">
                Regular
              </Typography>
              <Typography variant="caption1" weight="medium">
                Medium
              </Typography>
            </div>
        
            <div className="p-5 space-y-2 border-r pr-7 border-gray-400">
              <Typography variant="caption3" weight="medium">
                Caption2
              </Typography>
              <Typography variant="caption2" weight="regular">
                Regular
              </Typography>
              <Typography variant="caption2" weight="medium">
                Medium
              </Typography>
            </div>
        
            <div className="p-5 space-y-2 border-r pr-7 border-gray-400">
              <Typography variant="caption3" weight="medium">
                Caption3
              </Typography>
              <Typography variant="caption3" weight="regular">
                Regular
              </Typography>
              <Typography variant="caption3" weight="medium">
                Medium
              </Typography>
            </div>
        
            <div className="p-5 space-y-2 border-r pr-7 border-gray-400">
              <Typography variant="caption3" weight="medium">
                Caption4
              </Typography>
              <Typography variant="caption4" weight="regular">
                Regular
              </Typography>
              <Typography variant="caption4" weight="medium">
                Medium
              </Typography>
            </div>
          </div>
        </div>
        </div>
        
        {/* Spinners */}
        <div className="flex items-start gap-7">
          <div className="space-y-2">
            <Typography variant="caption2" weight="medium">
              Spinners
            </Typography>
            <div className="flex items-center gap-2 p-5 border border-gray-400 rounded">
              <Spinner size="small" />
              <Spinner />
              <Spinner size="large" />
            </div>
          </div>
        </div>
        
        {/* Logo */}
        <div className="flex items-start gap-7">
          <div className="space-y-2">
            <Typography variant="caption2" weight="medium">
              Logo
            </Typography>
            <div className="flex items-center gap-2 p-5 border border-gray-400 rounded">
              <Logo size="very-small" />
              <Logo size="small" />
              <Logo />
              <Logo size="large" />
            </div>
          </div>
        </div>
        
        {/* Avatar */}
        <div className="flex items-start gap-7">
          <div className="space-y-2">
            <Typography variant="caption2" weight="medium">
              Avatar
            </Typography>
            <div className="flex items-center gap-2 p-5 border border-gray-400 rounded">
              <Avatar size="small"  src="/assets/images/membre.jpg" alt="Avatar de client ..."/>
              <Avatar  src="/assets/images/membre.jpg" alt="Avatar de client ..."/>
              <Avatar size="large"  src="/assets/images/membre.jpg" alt="Avatar de client ..."/>
            </div>
          </div>
        </div>
        
        
        {/* Buttons */}
        <div className="space-y-2">
        <Typography variant="caption2" weight="medium">
          Buttons
        </Typography>
        <div className="flex items-center gap-2 p-5 border border-gray-400 rounded">
          <div className="space-y-2">
            <Typography variant="caption3" weight="medium">
              Small
            </Typography>
            <div className="space-y-4">
              <div className="flex flex-wrap items-center gap-2">
                <Button isLoading size="small">Accent</Button>
                <Button isLoading size="small" icon={{ icon: RiUser6Fill}} iconPosition="left">Accent</      Button>
                <Button isLoading size="small" icon={{ icon: RiUser6Fill}}>Accent</Button>
                <Button isLoading variant="secondary" size="small">Secondary</Button>
                <Button isLoading variant="outline" size="small">Button</Button>
                <Button isLoading variant="disabled" size="small">Button</Button>
                <Button isLoading variant="ico" size="small" icon={{ icon: RiUser6Fill}} />
              </div>
              <div className="flex flex-wrap items-center gap-2">
                <Button size="small">Accent</Button>
                <Button size="small" icon={{ icon: RiUser6Fill}} iconPosition="left">Accent</Button>
                <Button size="small" icon={{ icon: RiUser6Fill}}>Accent</Button>
                <Button variant="secondary" size="small">Secondary</Button>
                <Button variant="outline" size="small">Button</Button>
                <Button variant="disabled" size="small">Button</Button>
                <Button variant="ico" size="small" icon={{ icon: RiUser6Fill}} />
              </div>
            </div>
          </div>
        </div>
        
        <div className="flex items-center gap-2 p-5 border border-gray-400 rounded">
          <div className="space-y-2">
            <Typography variant="caption3" weight="medium">
              Medium
            </Typography>
            <div className="space-y-4">
              <div className="flex flex-wrap items-center gap-2">
                <Button>Accent</Button>
                <Button variant="secondary">Secondary</Button>
                <Button variant="outline">Button</Button>
                <Button variant="disabled">Button</Button>
                <Button variant="ico" icon={{ icon: RiUser6Fill}} />
              </div>
            </div>
          </div>
        </div>
        
        <div className="flex items-center gap-2 p-5 border border-gray-400 rounded">
          <div className="space-y-2">
            <Typography variant="caption3" weight="medium">
              Large
            </Typography>
            <div className="space-y-4">
              <div className="flex flex-wrap items-center gap-2">
                <Button size="large">Accent</Button>
                <Button variant="secondary" size="large">Secondary</Button>
                <Button variant="outline" size="large">Button</Button>
                <Button variant="disabled" size="large">Button</Button>
                <Button variant="ico" size="large" icon={{ icon: RiUser6Fill}} />
                <Button variant="ico" size="large" icon={{ icon: RiUser6Fill}} iconTheme="secondary" />
                <Button variant="ico" size="large" icon={{ icon: RiUser6Fill}} iconTheme="gray" />
                <Button variant="ico" size="large" icon={{ icon: RiUser6Fill}} />
              </div>
            </div>
          </div>
        </div>
        </div>
        </Container>
      </Layout>

    </>
  )
}