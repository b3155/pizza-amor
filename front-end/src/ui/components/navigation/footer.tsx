import { Logo } from "@/ui/design-system/logo/logo"
import { Typography } from "@/ui/design-system/typography/typography"
import Link from "next/link"
import { Container } from "../container/container"
import { footerLinks } from "./app-links"
import { v4 as uuidv4 } from "uuid"
import { ActiveLink } from "./active-link"
import { FooterLinks } from "@/types/app-links"
import { LinkTypes } from "@/lib/link-type"
import { SocialNetworksButtons } from "./social-networks-buttons"

export const Footer = () => {

  const currentYear = new Date().getFullYear()
  
  const footerNavigationList = footerLinks.map((colomnLinks) => (
    <FooterLink key={uuidv4()} data={colomnLinks} />
  ))

  return (
    <div className="bg-primary">
      <Container className="flex flex-col justify-between pt-16 gap-y-8 xl:flex-row">
        <div className="flex flex-col items-center gap-5">
          <Link href="/">
            <Logo 
            size="large"
            />
          </Link>
          <Typography
          variant="caption1"
          theme="white"
          weight="medium"
          >
            Amor Pizza
          </Typography>
          <Typography
          variant="caption1"
          theme="gray"
          >
            74000 Annecy
          </Typography>
          <Typography
          variant="caption2"
          theme="gray"
          >
            Tel: ++33 000 000 000
          </Typography>
          <Typography
          variant="caption2"
          theme="gray"
          >
            Email: amorpizza@gmail.com
          </Typography>
        </div>
        <div className="flex flex-col gap-7 sm:flex-row sm:justify-center">
          {footerNavigationList}
        </div>
        <div className="flex flex-col gap-5 sm:items-center">
          <Typography
          variant="caption1"
          theme="white"
          weight="medium"
          >
            HORAIRES
          </Typography>
          <Typography
          variant="caption2"
          theme="gray"
          >
            Lundi: Ferme
          </Typography>
          <Typography
          variant="caption2"
          theme="gray"
          >
            Mardi - Jeudi: 10 h 00 - 20 h 00
          </Typography>
          <Typography
          variant="caption2"
          theme="gray"
          >
            Mardi - Jeudi: 11 h 00 - 22 h 00
          </Typography>
        </div>
      </Container>
      <Container className="pt-9 pb-11 space-y-11">
        <hr className="text-gray-800" />
        <div className="flex flex-col-reverse items-center justify-between xl:flex-row">
          <div className="">
            <Typography
            variant="caption4"
            theme="gray"
            >
              {`Copyright ${currentYear} All Rights Reserved By - `}
              <span className="text-gray">Amor Pizza</span>
            </Typography>
          </div>
          <div className="pb-4">
            <SocialNetworksButtons theme="gray" />
          </div>
        </div>
      </Container>
    </div>
  )
}

interface footerLinkProps {
  data: FooterLinks
}

const FooterLink = ({data} : footerLinkProps) => {

  const linksList = data.links.map((link) => (
    <div key={uuidv4()}>
      {link.type === LinkTypes.INTERNAL  && (
        <ActiveLink href={link.baseUrl}>{link.label}</ActiveLink>
      )}
      {link.type === LinkTypes.EXTERNAL && (
        <a href={link.baseUrl} target="_blanc">{link.label}</a>
      )}
    </div>
  ))

  return (
    <div className="min-w-[190px]">
      <Typography
      variant="caption2"
      theme="white"
      weight="medium"
      className="pb-5"
      >
        {data.label}
      </Typography>
      <Typography
      variant="caption3"
      theme="gray"
      className="space-y-4"
      >
        {linksList}
      </Typography>
    </div>
  )
}