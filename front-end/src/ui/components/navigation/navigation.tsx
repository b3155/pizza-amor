import { useAuth } from "@/context/AuthUserContext"
import { Button } from "@/ui/design-system/button/button"
import { Logo } from "@/ui/design-system/logo/logo"
import { Typography } from "@/ui/design-system/typography/typography"
import Link from "next/link"
import { useState } from "react"
import { Container } from "../container/container"
import { ActiveLink } from "./active-link"

interface Props {

}

export const Navigation = ({} : Props) => {

  const [showMenu, setShowMenu] = useState(false)
  const {authUser, authUserIsLoading} = useAuth()
  
  console.log("authUser", authUser);
  console.log("authUserIsLoading", authUserIsLoading);
  
  

  return (
    <div className="border-b-2 border-secondary bg-white">
      <Container className="relative flex items-center justify-between py-1.5 gap-7">
        <Link href="/">
          <div className="flex items-center gap-2.5">
            <Logo size="small" />
            <div className="flex flex-col">
              <div className="hidden lg:block text-gray font-extrabold text-[24px]">
                Amor Pizza
              </div>
              <Typography 
                variant="caption4"
                component="span"
                theme="gray"
                className="hidden xl:block"
              >
                Trouve de l'inspiration & recois des feedbacks
              </Typography>
            </div>
          </div>
        </Link>
        <div className={`${showMenu ? "flex" : "hidden"} absolute top-full flex-col items-center gap-7 w-full left-0 py-5 bg-primary-200 z-30 md:flex md:relative md:flex-row md:justify-end md:bg-white`}>
          <Typography
            variant="caption3"
            component="div"
            className="flex flex-col md:flex-row items-center gap-7"
          >
            <ActiveLink href="/design-system">Design-system</ActiveLink>
            <ActiveLink href="/">Acueill</ActiveLink>
            <ActiveLink href="/menu">Menu</ActiveLink>
            <ActiveLink href="/about">Nous sommes</ActiveLink>
            <ActiveLink href="/contact">Contact</ActiveLink>
          </Typography>
          <div className="flex items-center gap-2">
            <Button baseUrl="/connexion" size="small">Connexion</Button>
            <Button baseUrl="/connexion/inscription" size="small" variant="secondary">Rejoindre</Button>
          </div>
        </div>
        <div className="bg-primary w-[40px] h-[40px] flex items-center justify-center rounded md:hidden">
          <button
          onClick={() => setShowMenu(!showMenu)}
          >
            <svg width="30px" height="30px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3. org/2000/svg">

              <g id="SVGRepo_bgCarrier" strokeWidth="0"/>

              <g id="SVGRepo_tracerCarrier" strokeLinecap="round" strokeLinejoin="round"/>

              <g id="SVGRepo_iconCarrier"> 
                <path d="M4 18L20 18" stroke="#f2f2f2" strokeWidth="2"             strokeLinecap="round"/> 
                <path d="M4 12L20 12"  stroke="#f2f2f2" strokeWidth="2"              strokeLinecap="round"/> 
                <path d="M4  6L20 6" stroke="#f2f2f2" strokeWidth="2"            strokeLinecap="round"/> 
              </g>

            </svg>
          </button>
        </div>
      </Container>
    </div>
  )
}