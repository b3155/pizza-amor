import { Layout } from "@/ui/components/layout/layout";
import { Seo } from "@/ui/components/seo/seo";
import { HomePageContainer } from "@/ui/modules/home-page/home-page.container";

export default function Home() {
  return (
    <>
      <Seo title="Amor Pizza" description="Description ..." />

      <Layout isDisplayBreadcrumbs={false}>
        <HomePageContainer />
      </Layout>

    </>
  );
}

