import { Layout } from "@/ui/components/layout/layout";
import { Seo } from "@/ui/components/seo/seo";
import { UserAccountContainer } from "@/ui/modules/user-profil/user-account/user-account.container";

export default function MonCompte() {
  return (
    <>
      <Seo 
        title="Mon espace" 
        description="Page mon compte" 
      />
      <Layout withSidebar>
        <UserAccountContainer />
      </Layout>
    </>
  );
}
