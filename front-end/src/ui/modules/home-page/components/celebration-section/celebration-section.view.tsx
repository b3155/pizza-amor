import { Container } from "@/ui/components/container/container";
import { Button } from "@/ui/design-system/button/button";
import { Typography } from "@/ui/design-system/typography/typography";
import Image from "next/image";

export const CelebrationSectionView = () => {
  return (
    <div>
      <Container className="py-24 space-y-12">
        <Typography variant="h2" component="h2" className="text-center">Soirees pizza et evenement</Typography>
        <div className="flex flex-col w-full gap-y-8 lg:gap-x-8 lg:flex-row">
          <div className="flex flex-col items-center justify-center space-y-10 bg-secondary p-12 xl:p-24">
            <Typography variant="body-lg" component="p">
              Du Plaisir pour tout le monde, la pizza fait la fete.<br></br> Organisez
              votre prochaine celebration avec Amor Pizza.
            </Typography>
            <Button >
              Contactez nous
            </Button>
          </div>
            <Image
              src="/assets/svg/gateaux.svg"
              alt="Illustration d'un gateaux"
              width={588}
              height={436}
              priority={true}
              className="w-full"
            />
        </div>
      </Container>
    </div>
  );
};
