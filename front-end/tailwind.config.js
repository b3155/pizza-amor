/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    colors: {
      white: "#f8f9fa",
      primary: {
        200: "#d7a1a7",
        300: "#da7f87",
        400: "#db515e",
        DEFAULT: "#dc3545",
        600: "#b91a29"
      },
      secondary: {
        200: "#fff2cd",
        300: "#ffe597",
        400: "#fcd04c",
        DEFAULT: "#ffc107",
        600: "#d7a100"
      },
      gray: {
        300: "#ede9db",
        400: "#b9c6d4",
        500: "#919fad",
        600: "#7a8591",
        700: "#555f68",
        800: "#393f46",
        DEFAULT: "#212529",
      },
      alert: {
        danger: "#ff4e4e",
        success: "#90da1a",
        warning: "#feb72f"
      }
    },
    fontSize: {
      '8xl': ["120px", {
        lineHeight: "120px",
        letterSpacing: "-6px",
        fontWeight: '500',
      }],
      '7xl': ["72px", {
        lineHeight: "80px",
        letterSpacing: "-4.5px",
        fontWeight: '600',
      }],
      '6xl': ["55px", {
        lineHeight: "60px",
        letterSpacing: "-2.5px",
        fontWeight: '500',
      }],
      '5xl': ["48px", {
        lineHeight: "54px",
        letterSpacing: "-1.6px",
        fontWeight: '500',
      }],
      '4xl': ["36px", {
        lineHeight: "44px",
        letterSpacing: "-1.2px",
        fontWeight: '500',
      }],
      '3xl': ["28px", {
        lineHeight: "34px",
        letterSpacing: "-0.8px",
        fontWeight: '500',
      }],
      '2xl': ["24px", {
        lineHeight: "30px",
        letterSpacing: "-1px",
        fontWeight: '400',
      }],
      'xl': ["24px", {
        lineHeight: "30px",
        letterSpacing: "-1px",
        fontWeight: '400',
      }],
      'lg': ["21px", {
        lineHeight: "30px",
        letterSpacing: "-0.8px",
        fontWeight: '500',
      }],
      'base': ["17px", {
        lineHeight: "25px",
        letterSpacing: "-0.7px",
        fontWeight: '400',
      }],
      'sm': ["15px", {
        lineHeight: "23px",
        letterSpacing: "-0.6px",
        fontWeight: '400',
      }],
      'caption1': ["20px", {
        lineHeight: "24px",
        letterSpacing: "-0.6px",
        fontWeight: '400',
      }],
      'caption2': ["18px", {
        lineHeight: "20px",
        letterSpacing: "-0.3px",
        fontWeight: '400',
      }],
      'caption3': ["16px", {
        lineHeight: "18px",
        letterSpacing: "-0.5px",
        fontWeight: '400',
      }],
      'caption4': ["13px", {
        lineHeight: "15px",
        letterSpacing: "-0.2px",
        fontWeight: '400',
      }]
    },
    borderRadius: {
      DEFAULT: '10px',
      full: "9999px"
    },
    extend: {},
  },
  plugins: [],
}

