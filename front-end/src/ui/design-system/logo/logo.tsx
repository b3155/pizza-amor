import Image from "next/image";
import clsx from "clsx"

interface Props {
  size?: "very-small" | "small" | "medium" | "large"
}

export const Logo = ({size = "medium"} : Props) => {

  let sizeLogo: string

  switch (size) {
    case "very-small":
      sizeLogo = "w-[34px] h-[34px]"
      break;
    case "small":
      sizeLogo = "w-[61px] h-[61px]"
      break;
    case "medium": //Defoult
      sizeLogo = "w-[88px] h-[88px]"
      break;
    case "large":
      sizeLogo = "w-[140px] h-[140px]"
      break;
  }

  return (
    <div className={clsx(sizeLogo, "relative bg-gray-400 rounded-full")}>
      <Image 
        fill
        src="/assets/images/pizza-amor.png"
        alt="Avatar de client ..."
        className="object-cover object-center rounded-full"
        priority={true}
      />
    </div>
    )
}