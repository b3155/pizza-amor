import { CallToActionView } from "./components/call-to-action/call-to-action.view"
import { CelebrationSectionView } from "./components/celebration-section/celebration-section.view"
import { FeaturedView } from "./components/featured/featured.view"
import { HeroTopView } from "./components/hero-top/hero-top.view"

export const HomePageView = () => {
  return (
    <>
      <HeroTopView />
      <FeaturedView />
      <CallToActionView />
      <CelebrationSectionView />
    </>
  )
}