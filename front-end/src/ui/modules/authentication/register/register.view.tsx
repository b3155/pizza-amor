import { FormType } from "@/types/form";
import { Container } from "@/ui/components/container/container";
import { Box } from "@/ui/design-system/box/box";
import { Typography } from "@/ui/design-system/typography/typography";
import Image from "next/image";
import Link from "next/link";
import { RegisterForm } from "./register.form";

interface Props {
  form: FormType;
}

export const RegisterView = ({ form }: Props) => {
  return (
    <Container className="py-8 md:py-24">
      <div className="flex flex-col space-y-12 items-center justify-between py-12 sm:flex-row sm:space-y-0 sm:py-24">
        <div className="relative w-full h-[200px] sm:w-[190px] sm:h-[120px]">
          <Image
            fill
            src="/assets/svg/salad.svg"
            alt="Illustration d'une salad"
            className="object-cover"
            priority={true}
          />
        </div>
        <div className="relative w-full h-[200px] sm:w-[190px] sm:h-[120px]">
          <Image
            fill
            src="/assets/svg/gateaux.svg"
            alt="Illustration d'un gateaux"
            className="object-cover"
          />
        </div>
      </div>
      <div className="flex items-center max-w-3xl mx-auto">
        <Box>
          <div className="flex items-center justify-center">
            <Typography variant="h5" component="h1">
              Inscription
            </Typography>
          </div>
          <RegisterForm form={form} />
          <div className="flex items-center justify-center gap-2 sm:justify-end">
            <Typography variant="caption4" component="span" theme="gray">
              Tu as déja un compte ?
            </Typography>
            <Typography variant="caption4" component="span" theme="primary">
              <Link href="/connexion">Connexion</Link>
            </Typography>
          </div>
          <Typography
            variant="caption4"
            theme="gray"
            className="max-w-md mx-auto space-y-1 text-center mt-4"
          >
            <div>En T'inscrivant, tu acceptes les</div>
            <div>
              <Link href="#" className="text-gray-600 underline">
                Condition d'utilisation
              </Link>
              {" "}et la {" "}
              <Link href="#" className="text-gray-600 underline">
                Politique de confidentialité
              </Link>
              .
            </div>
          </Typography>
        </Box>
      </div>
    </Container>
  );
};
