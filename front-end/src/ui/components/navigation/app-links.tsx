import { AppLinks } from "@/types/app-links";
import { RiInstagramFill, RiTwitterFill, RiYoutubeFill } from "react-icons/ri";

const footerApplicationLinks: AppLinks[] = [
  {
    label: "Accueil",
    baseUrl: "/",
    type: "internal"
  },
  {
    label: "Menu",
    baseUrl: "/#",
    type: "internal"
  },
  {
    label: "Qui sommes nous",
    baseUrl: "/#",
    type: "internal"
  },
  {
    label: "Contact",
    baseUrl: "/#",
    type: "internal"
  },
  {
    label: "Accueil",
    baseUrl: "/#",
    type: "internal"
  },
  {
    label: "Accueil",
    baseUrl: "/#",
    type: "internal"
  },
]

const footerUsersLinks: AppLinks[] = [
  {
    label: "Mon espaces",
    baseUrl: "/#",
    type: "internal"
  },
  {
    label: "Conexion",
    baseUrl: "/connexion",
    type: "internal"
  },
  {
    label: "Inscription",
    baseUrl: "/connexion/inscription",
    type: "internal"
  },
  {
    label: "Mot de passe oublie",
    baseUrl: "/connexion/mots-de-passe-perdu",
    type: "internal"
  },
  {
    label: "Mon espaces",
    baseUrl: "/#",
    type: "internal"
  },
]

const footerInformationLinks: AppLinks[] = [
  {
    label: "CGU",  
    baseUrl: "/#",
    type: "internal"
  },
  {
    label: "Confidentialite",  
    baseUrl: "/#",
    type: "internal"
  },
  {
    label: "A propos",  
    baseUrl: "/#",
    type: "internal"
  },
  {
    label: "Contact",  
    baseUrl: "/#",
    type: "internal"
  },
]

export const footerSocialNetworksLinks: AppLinks[] = [
  {
    label: "Youtube",
    baseUrl: "https://www.youtube.com",
    type: "external",
    icon: RiYoutubeFill
  },
  {
    label: "Twitter",
    baseUrl: "https://www.twitter.com",
    type: "external",
    icon: RiTwitterFill
  },
  {
    label: "Instagram",
    baseUrl: "https://www.instagram.com",
    type: "external",
    icon: RiInstagramFill
  },
]

export const footerLinks = [
  {
    label: "App",
    links: footerApplicationLinks
  },
  {
    label: "Utilisaturs",
    links: footerUsersLinks
  },
  {
    label: "Information",
    links: footerInformationLinks
  },
  // {
  //   label: "Reseaux",
  //   links: footerSocialNetworksLinks
  // }
]