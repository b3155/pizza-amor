import { FormType } from "@/types/form";
import { Button } from "@/ui/design-system/button/button";
import { Input } from "@/ui/design-system/forms/input";

interface Props {
  form: FormType;
}

export const LoginForm = ({ form }: Props) => {
  const { onSubmit, errors, isLoading, register, handleSubmit } = form;

  // console.log("form", form);

  return (
    <form onSubmit={handleSubmit(onSubmit)} className="pt-8 pb-5 space-y-5">
      <Input
        isLoading={isLoading}
        placeholder="Votre email"
        type="email"
        register={register}
        errors={errors}
        errorMsg="Tu dois renseigner ce champ"
        id="email"
      />
      <Input
        isLoading={isLoading}
        placeholder="Votre password"
        type="password"
        register={register}
        errors={errors}
        errorMsg="Tu dois renseigner ce champ"
        id="password"
      />
      <Button isLoading={isLoading} type="submit" fullWidth>
        Connexion
      </Button>
    </form>
  );
};
