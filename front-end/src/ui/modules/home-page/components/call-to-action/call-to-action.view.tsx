import { Container } from "@/ui/components/container/container";
import { Button } from "@/ui/design-system/button/button";
import { Typography } from "@/ui/design-system/typography/typography";

export const CallToActionView = () => {
  return (
    <div className="bg-primary">
      <Container className="flex flex-col items-center py-24 space-y-10">
        <Typography variant="h2" component="h2">
          Qui sommes nous
        </Typography>
        <div className="max-w-4xl">
          <Typography variant="body-lg" component="p">
            Depuis 1995, Amor Pizza est un lien de predilection de la famille
            pour ses pazzas a volonte et ses divertissement a volonte.
          </Typography>
          <Typography variant="body-lg" component="p">
            Decouvrez comment nous avons imagine le fantastique buffet de pizzas
            de Amor depuis le debut.
          </Typography>
        </div>
        <Button variant="gray">Lisez notre histoire</Button>
      </Container>
    </div>
  );
};
