import { Layout } from "@/ui/components/layout/layout";
import { Seo } from "@/ui/components/seo/seo";
import { ForgetPasswordContainer } from "@/ui/modules/authentication/forget-password/forget-password.container";

export default function ForgetPassword() {
  return (
    <>
      <Seo 
        title="Mots de passe perdu" 
        description="Page mots de passe perdu" 
      />
      <Layout>
        <ForgetPasswordContainer />
      </Layout>
    </>
  );
}