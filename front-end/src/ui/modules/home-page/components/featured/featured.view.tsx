import { Container } from "@/ui/components/container/container";
import { Button } from "@/ui/design-system/button/button";
import { Typography } from "@/ui/design-system/typography/typography";
import Image from "next/image";
import { features } from "process";
import { v4 as uuidv4 } from "uuid"

interface FeaturedListInterface {
  imagePath: string
  imageAlt: string
  title: string
  description: string
}

const featuredData: FeaturedListInterface[] = [
  {
    imagePath: "/assets/svg/pizzas.svg",
    imageAlt: "Ilustration",
    title: "Pizza",
    description: "Hello Pizza"
  },
  {
    imagePath: "/assets/svg/passta.svg",
    imageAlt: "Ilustration",
    title: "Pasta",
    description: "Hello Pizza"
  },
  {
    imagePath: "/assets/svg/salade.svg",
    imageAlt: "Ilustration",
    title: "Salade",
    description: "Hello Pizza"
  },
  {
    imagePath: "/assets/svg/dessert.svg",
    imageAlt: "Ilustration",
    title: "Dessert",
    description: "Hello Pizza"
  }
]

export const FeaturedView = () => {

  const featuredList = featuredData.map((featured) => (
    <div 
    key={uuidv4()}
    className="flex flex-col items-center justify-center max-w-[300px] bg-gray-300 rounded p-7"
    >
      <Typography variant="h3" component="h3" weight="medium" className="uppercase">
        {featured.title}
      </Typography>
      <div className="relative w-[175px] h-[165px] rounded my-6 p-10">
      <Image 
        fill
        src={featured.imagePath}
        alt={featured.imageAlt}
        className="object-scale-down blur-md"
        />
        <Image 
        fill
        src={featured.imagePath}
        alt={featured.imageAlt}
        className="object-scale-down"
        />
      </div>
      <Button>
        {`Voir les ${featured.title}s`}
      </Button>
    </div>
  ))

  return (
    <div className="">
      <Container className="flex flex-col gap-12 items-center py-24">
        <div className="flex flex-col items-center space-y-20">
          <Typography variant="h2" component="h2" className="text-center max-w-2xl">
            SI VOUS POUVEZ LE REVER, NOUS POUVONS LE FAIRE
          </Typography>
          <div className="flex flex-col items-center gap-12 max-w-5xl md:flex-row">
            <Typography variant="body-lg" component="p" className="">
              A notre carte, l”imagination est a l”honneur.Regalez-vous des
              yeux, avec nos dernieres innovations en matiere de pizzas, de
              pates, de salades et de desserts.
            </Typography>
            <Image 
              src="/assets/svg/salad.svg"
              alt=""
              width={190}
              height={116}
              className="w-full rounded"
            />
          </div>
        </div>
        <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-12">
          {featuredList}
        </div>
      </Container>
    </div>
  );
};
