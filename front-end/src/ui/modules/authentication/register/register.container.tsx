import { RegisterFormFielsType } from "@/types/form";
import { SubmitHandler, useForm } from "react-hook-form";
import { RegisterView } from "./register.view";
import { firebaseCreateUser, sendEmailVerificationProcedure } from "@/api/authentication";
import { toast } from "react-toastify";
import { useToggle } from "@/hooks/use-toggle";
import { firestoreCreateDocument } from "@/api/firestore";

export const RegisterContainer = () => {
  const { value: isLoading, setValue: setIsLoading, toggle } = useToggle();

  // console.log(isLoading);

  const {
    handleSubmit,
    formState: { errors },
    register,
    setError,
    reset,
  } = useForm<RegisterFormFielsType>();

  const handleCreateUserDocument = async (
    collectionName: string,
    documentID: string,
    document: object
  ) => {
    const { error } = await firestoreCreateDocument(
      collectionName,
      documentID,
      document
    );

    if(error) {
      toast.error(error.message)
      setIsLoading(false);
      return
    }
    toast.success("Bienvenue sur app de amor pizza");
    setIsLoading(false);
    reset();
    // send email confirmation procedure
    sendEmailVerificationProcedure()
  };

  const handleCreateUserAuthentication = async ({
    email,
    password,
    name,
    firstname
  }: RegisterFormFielsType) => {
    const { error, data } = await firebaseCreateUser(email, password);
    if (error) {
      setIsLoading(false);
      toast.error(error.message);
      return;
    }

    const userDocumentData = {
      email: email,
      name: name,
      firstname: firstname,
      uid: data.uid,
      creation_date: new Date()
    }

    handleCreateUserDocument("users", data.uid, userDocumentData)
    // console.log(data);
  };

  const onSubmit: SubmitHandler<RegisterFormFielsType> = async (formData) => {
    setIsLoading(true);
    // console.log("formData", formData);

    const { email, password } = formData;

    if (password.length <= 5) {
      setError("password", {
        type: "manuel",
        message: "Ton mot de passe doit comporte au minimum 6 caracteres",
      });
      return;
    }
    handleCreateUserAuthentication(formData);
  };

  return (
    <RegisterView
      form={{
        errors,
        register,
        handleSubmit,
        onSubmit,
        isLoading,
      }}
    />
  );
};
