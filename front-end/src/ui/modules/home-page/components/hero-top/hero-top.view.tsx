import { Container } from "@/ui/components/container/container";
import { Button } from "@/ui/design-system/button/button";
import { Typography } from "@/ui/design-system/typography/typography";
import Image from "next/image";

export const HeroTopView = () => {
  return (
    <Container className="flex flex-col py-40 space-y-8 lg:flex-row lg:space-y-0">
      <div className="flex flex-col w-full text-center items-center justify-center space-y-8 px-4 sm:px-10">
        <Typography variant="h1" component="h1" className="">
          TOUT EST PIZZABLE
        </Typography>

        <Typography
          variant="body-lg"
          theme="gray"
          component="p"
          className="mx-w-[15px]"
        >
          Vivez de merveilleux exploits de creativite en matiere de pizza
          uniquement chez Amor Pizza.
        </Typography>

        <Button baseUrl="">Commande maitenante</Button>
      </div>
      <Image
        src="/assets/svg/pizza-hero.svg"
        alt="Illustration d'une pizza de amor pizza"
        width={560}
        height={382}
        priority={true}
        className="w-full rounded"
      />
    </Container>
  );
};
