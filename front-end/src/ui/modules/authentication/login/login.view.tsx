import { FormType } from "@/types/form";
import { Container } from "@/ui/components/container/container";
import { Box } from "@/ui/design-system/box/box";
import { Typography } from "@/ui/design-system/typography/typography";
import Image from "next/image";
import Link from "next/link";
import { LoginForm } from "./login.form";

interface Props {
  form: FormType
}

export const LoginView = ({form} : Props) => {
  return (
    <Container className="py-8 md:py-24">
      <div className="flex flex-col space-y-12 items-center justify-between py-12 sm:flex-row sm:space-y-0 sm:py-24">
        <div className="relative w-full h-[200px] sm:w-[190px] sm:h-[120px]">
          <Image
            fill
            src="/assets/svg/salad.svg"
            alt="Illustration d'une salad"
            className="object-cover"
            priority={true}
          />
        </div>
        <div className="relative w-full h-[200px] sm:w-[190px] sm:h-[120px]">
          <Image
            fill
            src="/assets/svg/gateaux.svg"
            alt="Illustration d'un gateaux"
            className="object-cover"
          />
        </div>
      </div>
      <div className="flex items-center max-w-3xl mx-auto">
        <Box>
          <div className="flex items-center justify-center">
            <Typography variant="h5" component="h1">
              Connexion
            </Typography>
          </div>
          <LoginForm form={form} />
          <div className="flex items-center justify-between">
            <Typography variant="caption4" component="span" theme="primary">
              <Link href="/connexion/mots-de-passe-perdu">
                Mot de passe perdu ?
              </Link>
            </Typography>
            <div className="flex items-center gap-2">
              <Typography variant="caption4" component="span" theme="gray" className="hidden sm:block">
                Tu n'as pas de compte ?
              </Typography>
              <Typography variant="caption4" component="span" theme="primary">
                <Link href="/connexion/inscription">S'inscrire</Link>
              </Typography>
            </div>
          </div>
        </Box>
      </div>
    </Container>
  );
};
